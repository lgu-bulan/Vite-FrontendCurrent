import React, { useEffect, useState } from "react";
import { Carousel } from "react-bootstrap";
import "./jumbotron.scss";
function Jumbotron() {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    console.log(`check Loaded txt animation ${loaded}`);
    setLoaded(true);
  }, []);

  return (
    <div>
      <Carousel fade className="carousel-home">
        <Carousel.Item>
          <div className="carousel-container">
            <span className={`crsl-spn1 ${loaded ? "loaded" : ""}`}>
              <b>Endowed </b>with{" "}
            </span>
            <span className={`crsl-spn2 ${loaded ? "loaded" : ""}`}>
              <b>Natural</b> resources
            </span>
            <span className={`crsl-p ${loaded ? "loaded" : ""}`}>and wonderful sceneries.</span>
            <button className={`crs1-btn ${loaded ? "loaded2" : ""}`}>Explore!</button>
          </div>
          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default Jumbotron;
