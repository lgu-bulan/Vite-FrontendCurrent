import React from 'react';
import "./footer.css";

function Footer() {
  return (

        <div id="footerContainer">
          <div>
          <p style={{ textAlign: "center"}}>©Copyright 2023 by MARK ELVIN SIMORA</p>
          </div>
        </div>

  )
}

export default Footer