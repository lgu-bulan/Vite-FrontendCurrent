import React from 'react'
import { Document, Page } from 'react-pdf';
function PdfFileViewer({ fileUrl }) {

    const handleDownload = () => {
        window.open(fileUrl, '_blank');
      };
  return (
    <div>
    <Document file={fileUrl}>
      <Page pageNumber={1} />
    </Document>
    <button onClick={handleDownload}>Download</button>
  </div>
  )
}

export default PdfFileViewer