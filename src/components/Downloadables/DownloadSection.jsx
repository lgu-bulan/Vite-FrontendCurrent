import React, { useEffect, useState } from "react";
import {
  CalendarOutlined,
  DownloadOutlined,
  FileImageTwoTone,
  FilePdfTwoTone,
  FileWordTwoTone,
  HddOutlined,
} from "@ant-design/icons";
import { Col, Divider, Input, Pagination, Row, Spin } from "antd";
import queryString from "query-string";
import { AiOutlineInbox } from "react-icons/ai";
import { Link, useLocation, useNavigate } from "react-router-dom";
import useFetchDataDL from "../../hooks/useFetchDataDL";
import SearchBarDL from "../SearchBar/SearchBarDL/SearchBarDL";
import "./Downloadables.scss";
const { Search } = Input;

function DownloadSection({ url, category }) {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const perPage = 10;
  const [currentPage, setCurrentPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState(queryParams.get("search") || "");
  const [sortBy, setSortBy] = useState(queryParams.get("sort") || "date");
  const navigate = useNavigate();
  // const url = "http://localhost:1337/api/downloadables/?populate=*";
  const { dataResult, isLoading, setIsLoading } = useFetchDataDL({
    url,
    category,
  });

  let option = [
    {
      label: "OrderBy",
      options: [
        {
          label: "Date",
          value: "date",
        },
        {
          label: "Larger File Size",
          value: "LargeFileSize",
        },
        {
          label: "Smaller File Size",
          value: "SmallFileSize",
        },
      ],
    },
  ];

  const updateURLParams = (params) => {
    const updatedParams = { ...params, page: currentPage };
    const searchParams = new URLSearchParams(updatedParams);
    const newUrl = `?${searchParams.toString()}`;
    navigate(newUrl, { replaceState: true });
    window.history.replaceState({ path: newUrl }, "", newUrl);
  };

  // popstate
  const handlePopState = (event) => {
    if (!event.state) {
      // If the popstate event doesn't have a state property, it means it was triggered by a back or forward action
      const params = new URLSearchParams(window.location.search);
      const searchQuery = params.get("search") || "";
      const sort = params.get("sort") || "";

      setSearchQuery(searchQuery);
      setSortBy(sort);
      setCurrentPage(1);
      setIsLoading(true);
    }
  };

  const handleSubmitSearch = (e) => {
    e.preventDefault();
    setCurrentPage(1);
    updateURLParams({ sort });
  };

  const handleSearchChange = (value) => {
    setSearchQuery(value);
    updateURLParams({ search: value });
  };

  const handleSortChange = (value) => {
    setSortBy(value);
    updateURLParams({ sort: value });
  };
  // Filter
  const filteredData = dataResult.filter((data) => {
    const title = data.attributes.TITLE ? data.attributes.TITLE.toLowerCase() : "";
    const searchQueryLowerCase = searchQuery ? searchQuery.toLowerCase() : "";
    return title.includes(searchQueryLowerCase);
  });

  // Pagination
  const handlePaginationChange = (page) => {
    setCurrentPage(page);
    const queryParams = queryString.parse(location.search);
    const updatedQueryParams = { ...queryParams, page: page.toString() };
    const searchString = queryString.stringify(updatedQueryParams);
    navigate({ search: searchString });
  };
  // Convert Size
  const convertToSize = (Size) => {
    if (Size < 1024) {
      return Size + " KB";
    } else if (Size >= 1024 && Size < 1048576) {
      return (Size / 1024).toFixed(2) + " MB";
    } else if (Size >= 1048576) {
      return (Size / 1048576).toFixed(2) + " GB";
    }
  };

  // Sort
  const sortedData = [...filteredData].sort((a, b) => {
    const fileSizeA = a.attributes.Document?.data?.attributes?.size || 0;
    const fileSizeB = b.attributes.Document?.data?.attributes?.size || 0;

    if (sortBy === "date") {
      return new Date(b.attributes.updatedAt) - new Date(a.attributes.updatedAt);
    } else if (sortBy === "LargeFileSize") {
      return fileSizeB - fileSizeA;
    } else if (sortBy === "SmallFileSize") {
      return fileSizeA - fileSizeB;
    }
    return 0;
  });

  useEffect(() => {
    const queryParams = queryString.parse(location.search);
    const page = queryParams.page ? parseInt(queryParams.page) : 1;
    setCurrentPage(page);

    setTimeout(() => {
      setIsLoading(false);
    }, 1200);
    // setTimeout(() => {
    //   setIsLoading(false);
    // }, 1200);

    // Add the event listener for the popstate event
    window.addEventListener("popstate", handlePopState);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("popstate", handlePopState);
    };
  }, [location.search]);

  return (
    <div className="card-downloadbale-body py-4">
      <div className="card-downloadbale-container mx-auto h-100">
        <div className="header-card-download">
          <form className="Filter-Menu" onSubmit={handleSubmitSearch}>
            <SearchBarDL onSearch={handleSearchChange} sortOptions={option} sortBy={sortBy} handleSortChange={handleSortChange} />
          </form>
        </div>

        {isLoading ? (
          <div
            className="loading-spinner"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "80vh",
            }}
          >
            <Spin size="large" />
          </div>
        ) : sortedData.length === 0 ? (
          <div className="Empty-result-downloadables">
            <AiOutlineInbox className="icon-nodata" />
            <h1>No files available.</h1>
          </div>
        ) : (
          // </div>
          <div className="result-downloadables">
            {sortedData?.slice((currentPage - 1) * perPage, currentPage * perPage).map((data) => (
              <Row xs={1} md={3} lg={3} className="download-Cards d-flex align-items-center" key={data.id}>
                <Col xs={24} md={2} lg={2}>
                  <div className="mimetype-icons-div d-flex align-items-center justify-content-center p-2 m-2">
                    {data.attributes.Document.data.attributes.ext === ".pdf" ? (
                      <FilePdfTwoTone className="icon-downloadables" twoToneColor="#f5222d" />
                    ) : data.attributes.Document.data.attributes.ext === ".docx" ? (
                      <FileWordTwoTone className="icon-downloadables" twoToneColor="#1677ff" />
                    ) : data.attributes.Document.data.attributes.ext === ".jpg" || "image/png" ? (
                      <FileImageTwoTone className="icon-downloadables" />
                    ) : (
                      <></>
                    )}
                  </div>
                </Col>

                <Col xs={24} md={16} lg={16}>
                  <div className="card_downloable_title_div">
                    <p className="download-title" style={{ margin: "0" }}>
                      {data.attributes.TITLE}
                    </p>
                  </div>
                  <Divider className="mx-2 my-0 divider-dl" />
                  <div className="card_downloable_info d-flex align-items-center">
                    <CalendarOutlined style={{ marginRight: "8px", fontSize: "18px" }} />{" "}
                    <p className="pe-3">Date Uploaded: {data.attributes.Document.data.attributes.updatedAt.slice(0, 10)}</p>
                    <HddOutlined style={{ marginRight: "8px", fontSize: "18px" }} />{" "}
                    <p className="pe-3">
                      FileSize:
                      <span>{convertToSize(data.attributes.Document.data.attributes.size)}</span>
                    </p>
                  </div>
                </Col>

                <Col xs={24} md={6} lg={6}>
                  <div className="card_downloadble_action">
                    <Link to={data.attributes.Document.data.attributes.url} target="_blank" style={{ textDecoration: "none" }}>
                      <button
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          padding: "",
                        }}
                      >
                        Download
                        <DownloadOutlined
                          style={{
                            marginLeft: "12px",
                            fontSize: "1.2rem",
                            fontWeight: "bolder",
                          }}
                        />
                      </button>
                    </Link>
                  </div>
                </Col>
              </Row>
            ))}
          </div>
        )}

        <div className="footer-card-download">
          {" "}
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              fontWeight: "bold",
            }}
          >
            <Pagination
              current={currentPage}
              pageSize={perPage}
              total={sortedData.length}
              onChange={handlePaginationChange}
              showTotal={(total, range) => `${range[0]}-${range[1]} of ${total} items`}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default DownloadSection;
