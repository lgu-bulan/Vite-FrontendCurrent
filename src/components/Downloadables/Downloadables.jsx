import React from 'react'
import './Downloadables.scss'
import TabsComponent from '../Tab/TabsComponent'
import DownloadSection from './DownloadSection';




function Downloadables() {
    const tabItems = [
        {
          label: "PDF/FILE",
          children: <Upload2 />,
        },
        {
          label: "MEDIA",
          children: <CustomComponent1 />,
        },
        {
          label: "Custom Component Tab",
          children: 'test',
        },
      ];
  return (
    <div className='container-Downloadables'>
        <div>
            <TabsComponent />
        </div>
    </div>
  )
}

export default Downloadables