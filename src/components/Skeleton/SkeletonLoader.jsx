import React from "react";
import { Skeleton } from "antd";

function SkeletonLoader({ numberSkeleton }) {
  let skeletons = [];

  for (let i = 0; i < numberOfCards; i++) {
    skeletons.push(
      <Skeleton
        key={i}
        avatar
        active
        paragraph={{
          rows: 2,
        }}
      />
    );
  }

  return <>{skeletons}</>;
}

export default SkeletonLoader;
