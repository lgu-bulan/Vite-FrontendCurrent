import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import "./DateTime.scss";

function DateTime({ CustomClassName }) {
  const [dateTime, setDateTime] = useState(new Date());
  const [imageLoaded, setImageLoaded] = useState(false);
  const lcpImage = "https://res.cloudinary.com/dwfd6pzm5/image/upload/v1689678168/LOGOBULA_Nwebp_047f73d586.webp";

  useEffect(() => {
    const img = new Image();
    img.src = lcpImage;
    img.onload = () => {
      setImageLoaded(true);
    };
    const intervalId = setInterval(() => {
      setDateTime(new Date());
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  const day = dateTime.toLocaleDateString(undefined, {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  });
  const time = dateTime.toLocaleTimeString();

  return (
    <div className={`dateTimeBody ${CustomClassName}`}>
      <div className="container-dateTimeBody">
        <Row xs={1} md={2} className="banner-row">
          <Col xs={12} md={6} className="col-lgu-bulan-logo">
            <Row xs={1} md={2} className="banner_inner">
              <Col xs={4} md={3} className="col1_inner">
                {imageLoaded && (
                  <a href="/">
                    <img src={lcpImage} alt="LCP Image Bulan Logo" />
                  </a>
                )}
              </Col>
              <Col xs={8} md={9} className="col2_inner">
                <h1>BULAN-4706</h1>
                <p className="underline-banner"></p>
                <h4>Home of the Padaraw Festival</h4>
              </Col>
            </Row>
          </Col>
          <Col xs={12} md={6} className="col-date-time text-right">
            <div className="container-date-time">
              <span className="dateTime">
                {day} - {time}
              </span>
              <p className="p_OfficeHrs">Office Hours Weekdays 8am - 5pm</p>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default DateTime;
