import React, { useEffect, useState } from "react";
import { Input, Select } from "antd";
import "./SearchBarDL.scss";
const { Search } = Input;
const SearchBarDL = ({
  onSearch,
  disableCheck,
  sortOptions,
  sortBy,
  handleSortChange,
  defaultValue,
}) => {

  const handleSubmit = (event) => {
    event.preventDefault();
    filterData(searchQuery, sortBy);
  };

  return (
    <>
      <Search
        id="searchInputDL"
        className="dl-searchbar mx-4"
        placeholder="Search..."
        enterButton
        onSearch={onSearch}
        disabled={disableCheck}
      />

      <Select
        id="sortSelectDL"
        className="dl-dropdown mx-4"
        style={{ width: "200px" }}
        options={sortOptions}
        value={sortBy}
        onChange={handleSortChange}
        disabled={disableCheck}
        defaultValue={defaultValue}
      />
    </>
  );
};

export default SearchBarDL;
