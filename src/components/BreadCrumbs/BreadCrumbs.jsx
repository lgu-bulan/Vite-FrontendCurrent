import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";

function BreadCrumbs(props) {
  return (
    <Breadcrumb>
      {props.items.map((item, index) => (
        <Breadcrumb.Item key={index} href={item.url}>
          {item.label}
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
}

export default BreadCrumbs;
