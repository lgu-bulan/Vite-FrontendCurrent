import React from "react";
import { Tabs } from "antd";
import './TabsComponent.css'

function TabsComponent({ defaultActiveKey, type, size, tabItems}) {
  return (
    <div>
      <Tabs
        className="Tabs-Component"
        defaultActiveKey={defaultActiveKey}
        type={type}
        size={size}
        items={tabItems.map((tab, i) => ({
          ...tab,
          key: String(i + 1),
        }))}
      />
        
    </div>
  );
}

export default TabsComponent;
