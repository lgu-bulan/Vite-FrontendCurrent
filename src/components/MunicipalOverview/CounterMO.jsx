import React, { useEffect, useState, useRef, useCallback } from "react";
import "./MO.scss";
function CounterMO(props) {
  const [count, setCount] = useState(0);
  const prevCountRef = useRef();

  const counter = useCallback((minimum, maximum) => {
    for (let count = minimum; count <= maximum; count++) {
      if (count === maximum) {
        return;
      }
      setTimeout(() => {
        if (prevCountRef.current !== count) {
          setCount(count*5);
        }
      }, 1000 );
    }
  }, []);

  useEffect(() => {
    counter(0, props.value)
  }, [props.value, counter]);

  return (
    <div className="card--MOtitle">
      {count < props.value ? count : props.value}
    </div>
  );
}

export default CounterMO