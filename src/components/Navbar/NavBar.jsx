import React, { useState, useEffect } from "react";
import { Nav, NavDropdown, Navbar, Offcanvas } from "react-bootstrap";

import DateTime from "../Date-Time/DateTime";
// import { NavLink, useMatch } from 'react-router-dom';
import { Link, NavLink, useMatch } from "react-router-dom";
// import "./navBarTest.scss";
import "./NavBar.scss";

function NavBar() {
  const useActive = (path) => {
    const match = useMatch(path);
    // const match = useMatch(path, { exact="true": value.toString() });
    return match ? "active" : "";
  };

  return (
    // <div className={`NavBar-Container ${isSticky ? "sticky" : ""}`}>
    <div className="NavBar-Container">
      <DateTime />
      {/* <DateTime CustomClassName={isSticky ? "StickyBanner-Nav" : ""} /> */}

      <Navbar
        collapseOnSelect
        expand="md"
        className="Main-Navbar justify-content-end"
        // fixed="top"
      >
        <Navbar.Toggle className="me-3 " aria-controls="offcanvasNavbar-expand-sm" />
        <Navbar.Offcanvas
          className="main-navbar-offcanvas"
          id="offcanvasNavbar-expand-sm"
          aria-labelledby="offcanvasNavbarLabel-expand-sm"
          placement="end"
          // className="justify-content-end"
        >
          <Nav className="me-2 navbar-nav-offcanvass ms-3">
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id="offcanvasNavbarLabel-expand-sm">Bulan-4706</Offcanvas.Title>
            </Offcanvas.Header>

            <Offcanvas.Body className="nav-body">
              <NavLink className={`Main-Navbar-link px-2 ${useActive("/")}`} exact="true" to="/">
                Home
              </NavLink>
              {/* <NavLink className="Main-Navbar-link px-2" exact="true" to="/">
                Home
              </NavLink> */}

              {/* NEsted DropDOwn ABOUT BULAN Start*/}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="About Bulan"
                id="navbarScrollingDropdown"
                renderMenuOnMount={true} // cause shpw dropdown on render
              >
                <ul>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item ${useActive("/Overview")}`} exact="true" to="/Overview">
                      {/* <Link to="/Overview">Overview</Link> */}
                      Overview
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <div className="nav__aboutBulan__sub">
                      <NavDropdown
                        drop="end"
                        title="History"
                        id="navbarScrollingDropdown"
                        className="main-sub-menu"
                        renderMenuOnMount={true}
                        // style={{ padding: "0 ", margin: "0" }}
                      >
                        <ul>
                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/TownHistory")}`}
                              exact="true"
                              to="/TownHistory"
                            >
                              Town
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/BarangayHistory")}`}
                              exact="true"
                              to="/BarangayHistory"
                            >
                              Barangays
                            </NavLink>
                          </li>
                        </ul>
                      </NavDropdown>
                    </div>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.3">
                      Tourism
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.4">
                      Business
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.4">
                      Town Gallery
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                </ul>
              </NavDropdown>
              {/* NEsted DropDOwn ABOUT BULAN END*/}

              {/* NEsted DropDOwn OUR LGU Start*/}
              <NavDropdown className="px-2 nav-drpdwn-head" title="Our LGU" id="collasible-nav-dropdown" renderMenuOnMount={true}>
                <ul>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/">
                      Mission, Vision and <br></br>Quality Policy
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <div className="nav__OurLgu__sub__officeofthemayor">
                      {/* office of the mayor sub */}
                      <NavDropdown
                        drop="end"
                        title="Office of the Mayor"
                        id="collasible-nav-dropdown"
                        className="custom-dropdown main-sub-menu"
                        renderMenuOnMount={true}
                        // style={{ padding: "0 16px", margin: "0" }}
                      >
                        <ul>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/">
                              Mayor's Profile
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            {" "}
                            <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.2">
                              Executive Orders
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavDropdown
                              drop="end"
                              title="Executive & Legislative Agenda"
                              id="collasible-nav-dropdown"
                              className="custom-dropdown main-sub-menu"
                              renderMenuOnMount={true}

                              // style={{ padding: "4px 16px"}}
                            >
                              <ul>
                                <li>
                                  <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.1">
                                    Comprehensive Land and Use Plan
                                  </NavLink>
                                </li>
                              </ul>
                            </NavDropdown>
                          </li>
                          <NavDropdown.Divider />
                        </ul>
                      </NavDropdown>
                      {/* office of the mayor sub end */}
                    </div>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <div className="nav__OurLgu__sub__officeofthemayor__executiveagenda">
                      {/* Sanguniang Bayan sub */}

                      <NavDropdown
                        drop="end"
                        title="Sanguniang Bayan"
                        id="collasible-nav-dropdown"
                        className="custom-dropdown main-sub-menu"
                        renderMenuOnMount={true}
                        // style={{ color: "" }}
                      >
                        <ul>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.1">
                              Members
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.2">
                              Resolutions
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.1">
                              Ordinance
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                        </ul>
                      </NavDropdown>
                      {/* Sanguniang Bayan sub  END*/}
                    </div>
                  </li>
                  <NavDropdown.Divider />
                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/GovernmentServices">
                      Government Services
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                </ul>

                {/* <NavDropdown.Divider /> */}
              </NavDropdown>

              {/* NEsted DropDOwn OUR LGU END*/}

              {/*  News & Publications DropDown Start*/}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="News & Publications"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                <ul>
                  <NavDropdown.Divider />

                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/News">
                      News
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />

                  <li>
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.2">
                      Upcoming Events
                    </NavLink>
                  </li>
                  <NavDropdown.Divider />
                </ul>
              </NavDropdown>
              {/*  News & Publications DropDown End*/}

              {/*Transparency DropDOwn Start */}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="Transparency"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                {/* Bids & Awards Nested DropDown Start */}
                <div className="nav__Transparency__BidsAndAwards">
                  {/* Sanguniang Bayan sub */}
                  <NavDropdown.Divider />

                  {/* Sanguniang Bayan sub  END*/}
                  <ul>
                    <li>
                      {" "}
                      <NavDropdown
                        drop="end"
                        title="Bids and Awards"
                        id="collasible-nav-dropdown"
                        className="custom-dropdown main-sub-menu"
                        renderMenuOnMount={true}
                      >
                        {/* <NavDropdown.Item  as={Link} to="/Transparency/InvitationToBid"> */}
                        <ul>
                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/Transparency/InvitationToBid")}`}
                              exact="true"
                              to="/Transparency/InvitationToBid"
                            >
                              Invitation to Bid
                            </NavLink>
                          </li>

                          <NavDropdown.Divider />

                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/Transparency/RequestForQuotation")}`}
                              exact="true"
                              to="/Transparency/RequestForQuotation"
                            >
                              Request for Quotation
                            </NavLink>
                          </li>

                          <li>
                            <NavDropdown.Divider />
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/Transparency/NoticeToProceed")}`}
                              exact="true"
                              to="/Transparency/NoticeToProceed"
                            >
                              Notice to Proceed
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/Transparency/NoticeOfAward")}`}
                              exact="true"
                              to="/Transparency/NoticeOfAward"
                            >
                              Notice of Awards
                            </NavLink>
                          </li>
                          <NavDropdown.Divider />
                          <li>
                            <NavLink
                              className={`Main__NavDropdown__Item ${useActive("/Transparency/NoticeOfNegotiatedProcurement")}`}
                              exact="true"
                              to="/Transparency/NoticeOfNegotiatedProcurement"
                            >
                              Notice of Negotiated <br></br> Procurement
                            </NavLink>
                          </li>
                        </ul>
                      </NavDropdown>
                      <NavDropdown.Divider />
                    </li>
                    <li>
                      <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/">
                        Annual Report
                      </NavLink>
                      <NavDropdown.Divider />
                    </li>
                  </ul>
                </div>
                {/* Bids & Awards Nested DropDown End */}
              </NavDropdown>
              {/* Transparency DropDOwn End  */}

              {/* E-Services Start  */}
              <NavDropdown
                className="px-2 nav-drpdwn-head"
                title="e-Services"
                id="collasible-nav-dropdown"
                renderMenuOnMount={true}
              >
                <ul>
                  <li>
                    <NavDropdown.Divider />
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="/">
                      E-Community
                    </NavLink>
                  </li>
                  <li>
                    <NavDropdown.Divider />
                    <NavLink
                      className={`Main__NavDropdown__Item`}
                      exact="true"
                      to="https://prod9.ebpls.com/bulansorsogon/index.php/login"
                      target="_blank"
                    >
                      iBPLS (Integrated <br /> Business Processing <br />
                      and Licensing System)
                    </NavLink>
                  </li>
                  <li>
                    <NavDropdown.Divider />
                    <NavLink exact="true" to="https://pnpclearance.ph/" target="_blank">
                      Police Clearance
                    </NavLink>
                  </li>
                  <li>
                    <NavDropdown.Divider />
                    <NavLink className={`Main__NavDropdown__Item`} exact="true" to="#action/3.3">
                      Downloadables
                    </NavLink>
                  </li>
                </ul>
              </NavDropdown>
              {/* E-Services End  */}
              {/* <InputGroup className="mb-3" style={{ fontSize: "12px" }}>
                <Form.Control
                  placeholder="Recipient's username"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                />
                <Button variant="outline-secondary" id="button-addon2">
                  <i
                    className="fa-solid fa-magnifying-glass me-2"
                    style={{ fontSize: "2.005rem", color: "#fefefe" }}
                  ></i>
                </Button>
              </InputGroup> */}
            </Offcanvas.Body>
          </Nav>
        </Navbar.Offcanvas>
      </Navbar>
    </div>
  );
}

export default NavBar;
