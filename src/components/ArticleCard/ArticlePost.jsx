import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./ArticlePost.scss";
import { FaArrowRight } from "react-icons/fa6";
function ArticlePost() {
  const navigate = useNavigate();
  const [posts, setPosts] = useState([]);

  const fetchArticleData = async () => {
    try {
      const response = await axios.get("http://localhost:1337/api/posts?populate=*");
      const result = response.data.data;
      setPosts(result);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchArticleData();
  }, []);

  return (
    <div className="ArticleCard-Container">
      {posts
        .sort((a, b) => new Date(b.date_Posted) - new Date(a.date_Posted))
        .slice(0, 3)
        .map((data) => (
          <div
            className="Card-Article"
            key={data.id}
            onClick={() => {
              navigate(`/Articles/${data.id}`);
            }}
          >
            {/* header image */}
            <img
              className="Img_ArticleCard"
              variant="top"
              alt="Article Header Image"
              src={data.attributes?.header_img?.data?.attributes?.formats?.small?.url}
              loading="lazy"
              // src={data.header_img?}
            />

            {/* tags */}
            <div className="Category_Article">
              {data.attributes.category === "Latest News" ? (
                <div className="div_tag">
                  <span className="tag tag-teal">{data.attributes.category}</span>
                </div>
              ) : data.attributes.category === "Municipal News" ? (
                <div className="div_tag">
                  <span className="tag tag-purple">{data.attributes.category}</span>
                </div>
              ) : data.attributes.category === "Peso News" ? (
                <div className="div_tag">
                  <span className="tag tag-pink">{data.attributes.category}</span>
                </div>
              ) : (
                <></>
              )}

              {/* Contents */}
              <div className="Contents-CardArticle">
                <h2>{data.attributes.title}</h2>
                <div className="Article_Description" dangerouslySetInnerHTML={{ __html: data.attributes.ckeditor }}></div>
              </div>
              <Link className="lnk-readmore">
                Read more <FaArrowRight />
              </Link>
            </div>
          </div>
        ))}
    </div>
  );
}

export default ArticlePost;
