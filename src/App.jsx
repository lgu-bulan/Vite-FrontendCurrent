import { lazy, Suspense } from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { Spin } from "antd";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from "./components/Navbar/NavBar";
import Footer from "./components/Footer/pre-footer";
import Home from "./pages/Home";
import GotoTop from "./components/GtoTop/ButtonTop";

//About Bulan
const Overview = lazy(() => import("./pages/SubPages/OverView/Overview"));
const TownHistory = lazy(() => import("./pages/SubPages/History-Town/Barangay/HistoryTown"));
const BarangayHistory = lazy(() =>
  import("./pages/SubPages/History-Town/Barangay/HistoryBarangay")
);

// Our LGU
// News And Publication / Events
// import SpecificArticle from "./pages/SpecificArticle/SpecificArticle";
// Transparency
const InvitationToBid = lazy(() =>
  import("./pages/SubPages/Transparency/BidsAndAwards/InvitationToBid/InvitationToBid")
);
const NoticeOfAward = lazy(() =>
  import("./pages/SubPages/Transparency/BidsAndAwards/NoticeOfAwards/NoticeOfAwards")
);
const NoticeOfNegotiatedProcurement = lazy(() =>
  import("./pages/SubPages/Transparency/BidsAndAwards/NoticeOfAwards/NoticeOfAwards")
);
const NoticeToProceed = lazy(() =>
  import("./pages/SubPages/Transparency/BidsAndAwards/NoticeToProceed/NoticeToProceed")
);
const RequestForQuotation = lazy(() =>
  import("./pages/SubPages/Transparency/BidsAndAwards/RequestForQuotation/RequestForQuotation")
);
//
const GSO = lazy(() => import("../src/pages/Office/GSO/GSO.jsx"));
const GovernmentServices = lazy(() => import("../src/pages/SubPages/GovServices/GovServices"));
const ArticlesPage = lazy(() => import("../src/pages/ArticlePage/ArticlePage.jsx"));
const SingleArticlesPage = lazy(() => import("../src/pages/SinglePage/SingleArticle/SingleArticle"));
import ErrorPage from "./pages/ErrorPage/Errorpage";

function App() {
  return (
    <div className="App-Container">
      <Router>
        <NavBar />
        <Suspense
          fallback={
            <div className="SuspenseSpinner">
              <Spin size="large" />
            </div>
          }
        >
          <Routes>
            <Route path="/" element={<Home />} />
            {/*  About Bulan */}
            <Route path="/Overview" element={<Overview />} />
            <Route path="/TownHistory" element={<TownHistory />} />
            <Route path="/BarangayHistory" element={<BarangayHistory />} />

            {/* Our LGU */}

            {/* News And Publication / Events */}
            {/* article page here */}
            {/* <Route path="/Article/:id" element={<SpecificArticle />} /> */}

            {/* Transparency */}
            <Route path="/GovernmentServices/GSO" element={<GSO />} />
            <Route path="/Transparency/InvitationtoBid" element={<InvitationToBid />} />
            <Route path="/Transparency/NoticeOfAward" element={<NoticeOfAward />} />
            <Route
              path="/Transparency/NoticeOfNegotiatedProcurement"
              element={<NoticeOfNegotiatedProcurement />}
            />
            <Route path="/Transparency/NoticeToProceed" element={<NoticeToProceed />} />

            <Route path="/Transparency/RequestForQuotation" element={<RequestForQuotation />} />
            <Route path="/News" element={<ArticlesPage />} />
            <Route path="Articles/:id" element={<SingleArticlesPage />} />
            <Route path="/GovernmentServices" element={< GovernmentServices/>} />
            <Route path="/GovernmentServices:name" element={< GovernmentServices/>} />

            {/* error */}
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Suspense>
        <GotoTop />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
