import React from "react";
import "./GSO.scss";
import GsoHead from "../../../Img/OfficeHeads/GSOHEAD2.jpg";
import PageBanner from "../../../components/Banner/PageBanner";
function GSO() {
  return (
    <>
      <PageBanner title="GSO OFFICE" />
      <div className="GSO-Container">
        <div className="OfficeHeader">
          <img src={GsoHead} alt="GSO HEAD" className="OfficeHeadImg" />
        </div>
        <div className="Contents">
          <h1>The General Services Office of the LGU-Bulan</h1>
          <h5>BY: DARU THE EXCELBOY</h5>
          <br />
          <p>
            The General Services Office (GSO) plays a vital role in ensuring the efficient
            functioning of the Local Government Unit (LGU) of Bulan in the Philippines.
            Headed by Ms. Leny Tee-Magtangob, the GSO is responsible for various
            activities aimed at supporting the overall operations of the LGU. The duties
            and responsibilities of the GSO and provides an overview of their current
            activities.
          </p>
          <br />
          <h4>Centralization of Common Office Supplies</h4>
          <p>
            One of the primary responsibilities of the GSO is the centralization of common
            office supplies. This involves procuring, managing, and distributing essential
            office items such as stationery, paper, ink cartridges, and other materials
            necessary for daily administrative tasks. By centralizing the procurement
            process, the GSO ensures cost-effectiveness, standardization, and efficient
            allocation of office supplies across different departments and units within
            the LGU.
          </p>
          <br />
          <h4>Procurement and Inventory Management</h4>
          <p>
            The GSO is responsible for the procurement of goods and services required by
            the LGU. Additionally, the GSO maintains an accurate inventory of all procured
            items, ensuring that stock levels are monitored, and replenishments are made
            in a timely manner to avoid disruptions in operations.
          </p>
          <br />
          <h4>Property and Asset Management</h4>

          <p>
            Another significant duty of the GSO is the management of the LGU's property
            and assets. This involves maintaining an updated inventory of all
            government-owned assets, including land, buildings, vehicles, and equipment.
            The GSO ensures that these assets are properly utilized, safeguarded, and
            maintained. They may also be involved in the disposal or transfer of surplus
            or obsolete assets following established guidelines and procedures.
          </p>
          <br />

          <h4>Facilities and Maintenance</h4>

          <p>
            The GSO oversees the maintenance and repair of government facilities,
            including office buildings, public infrastructure, and vehicles. They
            coordinate with various departments and external contractors to ensure that
            maintenance activities are carried out promptly and efficiently. The GSO is
            also responsible for managing the utilities, such as electricity, water, and
            telecommunications services, to ensure uninterrupted operations.
          </p>
          <br />

          <h4>Records Management</h4>

          <p>
            Efficient record-keeping is essential for any government institution, and the
            GSO plays a critical role in this regard. They establish and implement systems
            for the proper classification, storage, and retrieval of official records,
            including financial documents, correspondences, and other important files. The
            GSO may also handle requests for information under the Freedom of Information
            (FOI) Act, ensuring transparency and accountability.
          </p>
          <br />

          <h4>Fleet Management</h4>

          <p>
            The GSO manages the LGU's fleet of vehicles, including allocation,
            maintenance, and monitoring. They ensure that vehicles are properly assigned
            to authorized personnel, and regular maintenance and repairs are conducted to
            keep them in optimal condition. Additionally, the GSO may coordinate with
            departments to plan and optimize the use of vehicles, ensuring cost-efficiency
            and adherence to transport regulations.
          </p>
          <br />
          <p style={{textIndent:"0"}}>
            At present, the GSO of the LGU of Bulan is located at a new building situated
            at the back of the SB (Sangguniang Bayan) Building. The GSO's current focus is
            on the centralization of common office supplies. By streamlining the
            procurement and distribution processes, the GSO aims to enhance efficiency,
            reduce costs, and ensure that all departments and units have access to the
            necessary supplies to carry out their respective duties effectively.
          </p>
        </div>
      </div>
    </>
  );
}

export default GSO;
