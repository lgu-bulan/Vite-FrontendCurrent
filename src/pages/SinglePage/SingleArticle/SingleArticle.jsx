import React, { useState, useEffect } from "react";
import axios from "axios";
import DOMPurify from "dompurify";
import useFetchDataSingleArticle from "../../../hooks/useFetchDataSingleArticle";
import { CalendarOutlined } from "@ant-design/icons";
import "./SingleArticle.scss";
function ProductDisplay() {
  const { articleData, error } = useFetchDataSingleArticle({});
  const [currentArticleIndex, setCurrentArticleIndex] = useState(articleData);
  const [listArticle, setListArticle] = useState(0);

  console.log(articleData.id);
  if (!articleData) {
    return <div>Article not found</div>;
  }

  const sanitizedContent = DOMPurify.sanitize(articleData?.attributes?.ckeditor);

  useEffect(() => {
    axios
      .get("http://localhost:1337/api/posts")
      .then((response) => {
        console.log(response.data.data);
        setListArticle(response.data.data);
      })
      .catch((error) => {
        console.error("Error fetching articles:", error);
      });
  }, []);

  const showPreviousArticle = () => {
    if (currentArticleIndex > 0) {
      setCurrentArticleIndex(currentArticleIndex - 1);
    }
  };

  const showNextArticle = () => {
    if (currentArticleIndex < listArticle.length - 1) {
      setCurrentArticleIndex(currentArticleIndex + 1);
    }
  };

  function ConvertDateTime(apiDate) {
    const dateObj = new Date(apiDate);

    const year = dateObj.getFullYear();
    const month = dateObj.getMonth() + 1;
    const day = dateObj.getDate();

    let hours = dateObj.getHours();
    const minutes = dateObj.getMinutes();

    const amOrPm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12 || 12;

    const formattedTime = `${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")} ${amOrPm}`;

    const formattedDateTime = `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")} ${formattedTime}`;

    return formattedDateTime;
  }
  //

  return (
    <div className="SingleArticleView">
      <div className="ArticleDisplay">
        <div className="HeaderImageContainer">
          <img src={articleData?.attributes?.header_img?.data?.attributes?.formats?.large?.url} alt="" />
        </div>

        <h1>{articleData?.attributes?.title}</h1>
        <div className="PublishedDate">
          <CalendarOutlined />
          <h5 className="m-0">Date Published:</h5>

          <h4>{ConvertDateTime(articleData?.attributes?.publishedAt)}</h4>
        </div>

        <div className="ArticleContents" dangerouslySetInnerHTML={{ __html: sanitizedContent }}></div>
      </div>
    </div>
  );
}

export default ProductDisplay;
