// import React from "react";
import { lazy, Suspense } from "react";
// import ArticleCard from "../components/ArticleCard/ArticlePost";
// import GovServices from "../components/GovServices/GovServices";
// import GotoTop from "../components/GtoTop/ButtonTop";
// import MunicipalOverview from "../components/MunicipalOverview/MunicipalOveriew";
// import Counter from "../components/MunicipalOverview/CounterMO";
// import Jumbotron from "../components/Jumbotron/Jumbotron";
// import Minfo from "../components/MInfo/MayorInfo";
// import Errorpage from "./ErrorPage/Errorpage";


const ArticleCard = lazy(() => import("../components/ArticleCard/ArticlePost"));
// const GovServices = lazy(() => import("../components/GovServices/GovServices"));
const MunicipalOverview = lazy(() => import("../components/MunicipalOverview/MunicipalOveriew"));
const Counter = lazy(() => import("../components/MunicipalOverview/CounterMO"));
const Jumbotron= lazy(() => import("../components/Jumbotron/Jumbotron"));
const Minfo= lazy(() => import("../components/MInfo/MayorInfo"));
const Errorpage= lazy(() => import("./ErrorPage/Errorpage"));
function Home() {
  return (
    <>
      {/* <Testing /> */}
      {/* <Errorpage /> */}
      {/* <GSO /> */}
      <Jumbotron />
      <ArticleCard />
      <Minfo />
      <MunicipalOverview />
      <Counter />
      {/* <GovServices /> */}
      {/* test */}
      {/* <ArticlePage /> */}
      {/* <GotoTop /> */}
      {/* <DownloadSection /> */}
    </>
  );
}

export default Home;
