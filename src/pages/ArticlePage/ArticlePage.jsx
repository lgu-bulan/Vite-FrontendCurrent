import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Col, Divider, Input, Row } from "antd";
import axios from "axios";
import { FaArrowRight } from "react-icons/fa6";
import PageBanner from "../../components/Banner/PageBanner";
import { CalendarOutlined } from "@ant-design/icons";
import "./ArticlePage.scss";
function ArticlePage() {
  const navigate = useNavigate();

  const [searchResult, setSearchResult] = useState([]);
  
  const fetchSearchResult = async () => {
    try {
      const response = await axios.get("http://localhost:1337/api/posts?populate=*");
      const fetchedData = response.data.data;

      setSearchResult(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  // Month
  const ConvertMonth = (dateString) => {
    const date = new Date(dateString);

    const options = {
      year: "numeric",
      month: "long",
      day: "2-digit",
    };

    const formattedDate = date.toLocaleDateString("en-US", options);

    return formattedDate;
  };

  //

  const handleSearchChange = (e) => {
    const newSearchQuery = e.target.value;
    setSearchQuery(newSearchQuery);
    // setCurrentPage(1);
    // updateURLParams({ sort: sortBy, search: newSearchQuery });
    // setIsLoading(true);
  };

  useEffect(() => {
    fetchSearchResult();
  }, []);
  return (
    <>
      <PageBanner title={"News and Publication"} />
      <div className="Articles-Container">
        <div className="div-Input-query">
          <h1>Result For "Lorem"</h1>
          <Input placeholder="Search..." className="ArticleSearchBar" />
        </div>
        <Row xs={1} md={2}>
          <Col xs={24} md={16}>
            <div className="ArticleDisplay">
              {/* <h1>Article Results</h1> */}
              {searchResult.map((data) => (
                <div
                  className="ResultCards"
                  key={data.id}
                  onClick={() => {
                    navigate(`/Articles/${data.id}`);
                  }}
                >
                  <div className="ThumbnailContainer">
                    <figure>
                      <img
                        className="ArticleThumbnail"
                        src={data?.attributes?.header_img?.data?.attributes?.formats?.thumbnail?.url}
                        alt="Thumbnail Result"
                      />
                    </figure>
                  </div>

                  <div className="ResultCardsInfo">
                    <h3>{data?.attributes?.title}</h3>
                    <div className="articles-card-info">
                      <CalendarOutlined />
                      <p className="DatePublish">{ConvertMonth(data?.attributes?.publishedAt)}</p>
                      <div className="ArticleCategory">
                        <strong>{data?.attributes?.category}</strong>
                      </div>
                    </div>
                    {/* <CalendarOutlined/> <p className="DatePublish">{ConvertMonth(data?.attributes?.publishedAt)}</p> */}
                    <div dangerouslySetInnerHTML={{ __html: data?.attributes?.ckeditor }} className="ResultCardDesc"></div>
                    <a href="#" className="cta-ReadMore">
                      Read More
                      <FaArrowRight className="arrow-right" />
                    </a>
                  </div>
                </div>
              ))}
            </div>
          </Col>
          <Col xs={24} md={8}>
            <div className="CategoryDisplay">
              <h1>Category</h1>
              <ul>
                <li>
                  <img
                    className="Category-Icon"
                    src={"https://res.cloudinary.com/dwfd6pzm5/image/upload/v1689302838/menu_2ad10aaa09.png"}
                    alt="cat-icon"
                  />
                  <a href="#">Latest News</a>
                </li>
                <Divider style={{ margin: "0" }} />
                <li>
                  <img
                    className="Category-Icon"
                    src={"https://res.cloudinary.com/dwfd6pzm5/image/upload/v1689302838/menu_2ad10aaa09.png"}
                    alt="cat-icon"
                  />
                  <a href="#">Municipal News</a>
                </li>
                <Divider style={{ margin: "0" }} />
                <li>
                  <img
                    className="Category-Icon"
                    src={"https://res.cloudinary.com/dwfd6pzm5/image/upload/v1689302838/menu_2ad10aaa09.png"}
                    alt="cat-icon"
                  />
                  <a href="#">Peso News</a>
                </li>
                <Divider style={{ margin: "0" }} />
              </ul>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default ArticlePage;
