import React from "react";
import PageBanner from "../../../../../components/Banner/PageBanner";
import DownloadSection from "../../../../../components/Downloadables/DownloadSection";

function RequestForQuotation() {
  const url = "http://localhost:1337/api/downloadables/?populate=*";
  const category = "Request for Quotation";
  return (
    <>
      <PageBanner title="Request For Quotation" />
      <DownloadSection url={url} category={category}/>
    </>
  );
}

export default RequestForQuotation;
