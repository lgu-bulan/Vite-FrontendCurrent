import React from "react";
import PageBanner from "../../../../../components/Banner/PageBanner";
import DownloadSection from "../../../../../components/Downloadables/DownloadSection";
function NoticeToProceed() {
  const url = "http://localhost:1337/api/downloadables/?populate=*";
  const category = 'Notice to Proceed'
  return (
    <>
      <PageBanner title="Notice Of Negotiated Procurement" />
      <DownloadSection url={url} category={category}/>
      {/* <DownloadSection url={url} /> */}
    </>
  );
}

export default NoticeToProceed;
