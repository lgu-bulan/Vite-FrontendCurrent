import React from "react";
import PageBanner from "../../../../../components/Banner/PageBanner";
import DownloadSection from "../../../../../components/Downloadables/DownloadSection";

function NoticeOfNegotiatedProcurement() {
  const url = "http://localhost:1337/api/downloadables/?populate=*";
  const category = 'Notice Of Negotiated Procurement'
  return (
    <>
      <PageBanner title="Notice Of Negotiated Procurement" />
      <DownloadSection url={url} category={category}/>
      {/* <DownloadSection url={url} /> */}
    </>
  );
}

export default NoticeOfNegotiatedProcurement;
