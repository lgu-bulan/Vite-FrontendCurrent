import React from "react";
import PageBanner from "../../../../../components/Banner/PageBanner";
import DownloadSection from "../../../../../components/Downloadables/DownloadSection";
import queryString from "query-string";
function NoticeOfAwards() {
  const url = "http://localhost:1337/api/downloadables/?populate=*";
  const category = 'Notice of Awards';
  return (
    <>
      <PageBanner title="Notice of Awards"/>
      <DownloadSection url={url} category={category}/>
      {/* <DownloadSection url={url}/> */}
    </>
  );
}

export default NoticeOfAwards;
