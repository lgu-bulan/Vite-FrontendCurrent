import React from 'react'
import DownloadSection from '../../../../../components/Downloadables/DownloadSection'
import PageBanner from '../../../../../components/Banner/PageBanner'
function InvitationToBid() {
    const url = "http://localhost:1337/api/downloadables/?populate=*";
    const category = "Invitation to Bid"
  return (
    <>
    <PageBanner title="Invitation to Bid"/>
    <DownloadSection url={url} category={category}/>
    {/* <DownloadSection url={url}/> */}
    </>
  )
}

export default InvitationToBid