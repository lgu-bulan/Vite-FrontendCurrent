import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
const useFetchDataSingleArticle = () => {
  const [articleData, setArticleData] = useState([]);
  const [error, setError] = useState([]);
  const { id } = useParams();
  useEffect(() => {
    axios
      .get(`http://localhost:1337/api/posts/${id}?populate=*`)
      .then((response) => {
        const posts = response.data.data;
        console.log(posts);

        // const article = posts.find((article) => article.id === parseInt(id));
        setArticleData(posts);
      })
      .catch((error) => {
        // Handle errors here
        setError(error);
      });
  }, []);

  return { articleData, error };
};

export default useFetchDataSingleArticle;
