import { useState, useEffect } from "react";
import axios from "axios";

function useFetchDataDL({ url, category, currentPage, searchQuery, sortBy }) {
  const [dataResult, setDataResult] = useState([]);
  const [totalItems, setTotalItems] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      try {

        const updatedUrl = `${url}`;
        // const updatedUrl = `${url}?category=${category}&page=${currentPage}&search=${searchQuery}&sort=${sortBy}`;
        const response = await axios.get(url);
        const data = response.data.data;
        const filteredData = data.filter((item) => {
          return item.attributes.category === category;
        });
        // console.log(data)
        setDataResult(filteredData);
        setTotalItems(data.length);
        setError(null);
      } catch (error) {
        setError(error);
      }

      setIsLoading(false);
    };

    fetchData();
  }, [url, category, currentPage, searchQuery, sortBy]);

  return { dataResult, totalItems, isLoading, error, setIsLoading };
}

export default useFetchDataDL;
