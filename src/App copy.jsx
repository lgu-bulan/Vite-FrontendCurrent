import { useState } from "react";
import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from "./components/Navbar/NavBarTest";
import Footer from "./components/Footer/pre-footer";
import Home from "./pages/Home";
import GotoTop from "./components/GtoTop/ButtonTop";
// 
import Overview from "./pages/SubPages/OverView/Overview"
import TownHistory from "./pages/SubPages/History-Town/HistoryTown"
import BarangayHistory from "./pages/SubPages/History-Town/Barangay/HistoryBarangay"
import ErrorPage from "./pages/ErrorPage/Errorpage"
const Layout = () => {
  return (
    <div className="app">
      <NavBar />
      <Outlet />
      <GotoTop />
      <Footer />
    </div>
  );
};

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/Overview",
        element: <Overview />,
      },
      {
        path: "/TownHistory",
        element: <TownHistory />,
      },
      {
        path: "/BarangayHistory",
        element: <BarangayHistory />,
      },
      // {
      //   path: "*",
      //   element: <ErrorPage />,
      // },
    ],
  },
]);

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
